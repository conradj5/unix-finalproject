typedef enum { literalFlag, variableFlag, operatorFlag } PTnodeFlag;

typedef struct {
	int operatorLiteral;
	int numOfOperands;
	struct PTnode1** operands;
} OperatorNode;

typedef struct PTnode1 {
	PTnodeFlag flag;
	union {
		int literalOrVariable;
		OperatorNode operator1;
	};
} PTnode;

extern int environment[26];
