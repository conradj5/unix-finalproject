
%{
#include "boolexp.h"
#include "boolexp.tab.h"
#include <stdio.h>
%}

%option yylineno
%x TV

%%

"["						{BEGIN TV;
							 return *yytext;}

<TV>[a-eg-su-z]		{yylval.environI = *yytext - 'a';
							return TVAR;}

<TV>"]"				{BEGIN 0;
							 return *yytext;}

[a-eg-su-z]				{yylval.environI = *yytext - 'a';
							 return VAR;}

[tf]						{yylval.literal = (*yytext == 't');
							 return LIT;}

<TV>[,]				{return *yytext;}
[~|&,()]				{return *yytext;}

<TV>[ \t\n]					{ }
<TV>.							{ }
[ \t\n]					{ }
.							{ }

%%

int yywrap(){
	return 1;
}
