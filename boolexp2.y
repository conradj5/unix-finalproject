%{
#include <stdlib.h>
#include <stdio.h>
#include "boolexp.h"
#include <stdarg.h>
#define SIZE 26

PTnode* newOperatorNode(int oper, int nops, ...);
PTnode* newLiteralOrVariableNode(int literalOrVariable, PTnodeFlag flag);
void freePTnode(PTnode* nodePtr);
int dfs(PTnode* nodePtr);

void yyerror(char* s);

int environment[SIZE];      /* environment */

extern int yylineno;

%}



%union {
   int literal;
   char environI;
   PTnode* nodePtr;
};

%token <literal> LIT
%token <environI> VAR
%type <nodePtr> declaration varlist exp sentence

%left '|'
%left '&'
%nonassoc '~'

%%

sentence: '(' declaration ',' exp ')'     {$$ = newOperatorNode(',', 2, $2, $4);}

declaration: '[' varlist ']'             {$$ = $2;}
           | '['']'                      { }

varlist: VAR                          	  {$$ = newLiteralOrVariableNode($1, variableFlag);}
       | VAR ',' varlist                 {$$ = newOperatorNode(',', 2,
																newLiteralOrVariableNode($1, variableFlag), $3);}

exp: exp '&' exp                         {$$ = newOperatorNode('&', 2, $1, $3);}
      | exp '|' exp                      {$$ = newOperatorNode('|', 2, $1, $3);}
      | '~' exp                          {$$ = newOperatorNode('~', 1, $2);}
      | LIT                              {$$ = newLiteralOrVariableNode($1, literalFlag);}
      | VAR                              {$$ = newLiteralOrVariableNode($1, variableFlag);}


%%

void yyerror(char* s){
   fprintf(stderr, "line %d: %s\n", yylineno, s);
}

int main(void){
   yyparse();
   return 0;
}

PTnode* newLiteralOrVariableNode(int lov, PTnodeFlag f){
   PTnode* nodePtr;

   if ((nodePtr = malloc (sizeof (*nodePtr))) == NULL)
      yyerror("out of memory");

   nodePtr->flag = f;
   nodePtr->literalOrVariable = lov;

   return nodePtr;
}

PTnode* newOperatorNode(int ol, int numOfOperands, ...){
   va_list ap;
   PTnode* nodePtr;

   int i;

   if ((nodePtr = malloc (sizeof (*nodePtr))) == NULL)
      yyerror("out of memory");

   nodePtr->flag = operatorFlag;
   nodePtr->operator1.operatorLiteral = ol;
   nodePtr->operator1.numOfOperands = numOfOperands;

   va_start(ap, numOfOperands);

   if (((nodePtr->operator1.operands = malloc (numOfOperands * sizeof (PTnode*)))) == NULL)
      yyerror("out of memory");

   for (i = 0; i < numOfOperands; i++){
      nodePtr->operator1.operands[i] = va_arg (ap, PTnode*);
   }
   va_end (ap);

   return nodePtr;
}

void freePTnode(PTnode* nodePtr) {

   int i;

   if (!nodePtr)
      return;

   if (nodePtr->flag == operatorFlag) {

      for (i = 0; i < nodePtr->operator1.numOfOperands; i++)
         freePTnode(nodePtr->operator1.operands[i]);
      if (nodePtr->operator1.operands)
         free(nodePtr->operator1.operands);
   }

   free(nodePtr); 
}
