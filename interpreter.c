#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "boolexp.h"
#include "boolexp.tab.h"

char* addStr(char* str, char* str2);

int dfs(PTnode* nodePtr, char* str) {
	int temp, temp_2;
	char tempChar;
	if (!nodePtr){
		return 0;
	}
	//printf("Flag: %d\n", (int)nodePtr->flag);
   switch (nodePtr->flag) {

      case literalFlag:
			//printf("FOUND LITERAL: %d\n", nodePtr->literalOrVariable);
         if (nodePtr->literalOrVariable)
				str = addStr(str, "t");
			else
				str = addStr(str, "f");
			return nodePtr->literalOrVariable;

      case variableFlag:
			//printf("FOUND Variable: %c\n", nodePtr->literalOrVariable + 'a');
			tempChar = 'a' + nodePtr->literalOrVariable;
			str = addStr(str, &tempChar);
         return environment[nodePtr->literalOrVariable];

      case operatorFlag:
			str = addStr(str, "(");
			//printf("FOUND Operator: %c\n", nodePtr->operator1.operatorLiteral);
          switch (nodePtr->operator1.operatorLiteral) {
             /* return the not of operator */
             case '~':
				 	str = addStr(str, "~");
					temp = !dfs(nodePtr->operator1.operands[0], str);
				   str = addStr(str, ")");  
					//printf("Found a ~ operand(s): %s", nodePtr->operator1.operands[0]);
				   return temp; 
             case '&':
				 	temp = dfs(nodePtr->operator1.operands[0], str); 
					str = addStr(str, " & ");
					temp_2 = dfs(nodePtr->operator1.operands[1], str);
					str = addStr(str, ")");
					return temp && temp_2;
			    case '|':
					temp = dfs(nodePtr->operator1.operands[0], str); 
					str = addStr(str, " | ");
					temp_2 = dfs(nodePtr->operator1.operands[1], str);
					str = addStr(str, ")");
					return temp || temp_2;

	  		}
		default:
			printf("NO FLAG FOUND!!\n");
	  
		
   }
	printf("RETURNING 0\n");
   return 0;
}

char* addStr(char* str, char* str2){
	int lengstr = strlen(str);
	int lengstr2 = strlen(str2);
	str = (char*)realloc(str, (lengstr+lengstr2+1)*sizeof(char));
	memcpy(str + lengstr, str2, lengstr2 + 1);
	return str;
}
