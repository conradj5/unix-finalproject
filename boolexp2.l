
%{
#include "boolexp.h"
#include "boolexp.tab.h"
%}

%option yylineno
%x TVAR

%%

[a-eg-su-z]				{yylval.environI = *yytext - 'a';
							 return VAR;}

[tf]						{yylval.literal = *yytext - 'a';
							 return LIT;}

[~|&,()[]]				{return *yytext;}

[ \t\n]					{ }
.							{ }

%%

int yywrap(){
	return 1;
}
