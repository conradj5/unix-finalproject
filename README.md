# Boolean Interpreter

This is a primitive interpreted lenguage based on boolean expressions.

To run this simply run `make all` and then run `./interpreter`.

Valid syntax can be found in the boolexpstdin.txt file. Variables must ba a single lower case character. Variables declared in the brackets have a value of true, while variables used in the logical statement and not declared in the brackets have a value of false.

e.g. `([], f | t)` would result in true, `([a], b)` would result in false.
