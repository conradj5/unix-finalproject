/*******************************************************************************
/
/      filename:  showheaders.l
/
/   description:  This is the lex file for showing headers in c and c++ files.
/
/        author:  Weiler, Matthew
/      login id:  FA_14_CPS444_07
/
/         class:  CPS 444
/    instructor:  Perugini
/    assignment:  Homework #9
/
/      assigned:  November 28, 2017
/           due:  December 5, 2017
/
/******************************************************************************/
oQuote [/][*]
lQuote [/][/]
cQuote [ \t\n]*[*][/]
angle <[^<>]*\.h>[ \t]*
quote ["][^"]+\.h["]
inc #include[ ]*
%{
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
extern int yy_flex_debug;
char* cbuf[50];
char* ubuf[50];
int cindex = 0;
int uindex = 0;
int bflag = 0;
int lflag = 0;
%}

%x Comment
%x toRecord

%%

{oQuote}                                 {BEGIN Comment; bflag = 1;}
{lQuote}                                 {BEGIN Comment; lflag = 1;}

{inc}                                    {BEGIN toRecord;}
<Comment>{inc}                           {BEGIN toRecord;}
<Comment>\n                              { if (lflag == 1){
                                             lflag = 0; 
                                             BEGIN 0;
                                          }}
<Comment>{cQuote}                        { if (bflag){
                                             bflag = 0; BEGIN 0;}}
<Comment>.                        { }
<toRecord>({angle}|{quote})               {if (lflag || bflag){
                                             cbuf[cindex] = strdup(yytext);
                                             cindex++;
                                             BEGIN Comment;
                                          } else {
                                             ubuf[uindex] = strdup(yytext);
                                             uindex++;
                                             BEGIN 0;
                                          }}
<toRecord>.                        { }
\n                                 { }
.                                  { } 

%%

int yywrap() {
   return 1;
}

void usage(){
   fprintf(stderr, "Usage: showheader [-cu] [file(s)...]");
}

int main(int argc, char** argv) {
   yy_flex_debug = 1;
   bool c = false;
   bool u = false;
   int opt, i;
   while((opt = getopt(argc, argv, "uc")) != -1) {
      switch (opt) {
      case 'u': u = true; break;
      case 'c': c = true; break;
      default:
         usage();
         exit(1);
      }
   }
   if (!c && !u){
      c = true;
      u = true;
   }

   for (i = 1; i < argc; i++){
      if (argv[i][0] != '-'){
         yyin = fopen(argv[i], "r");
         if (yyin == NULL){
            fprintf(stderr, "%s: Invalid file: %s\n", argv[0], argv[i]);
            usage();
            exit(2);
         }
      }
   }
   yylex();
   //fix if there is none to parse
   
   if (u){
      printf("Uncommented headers filename(s):\n");
      for (i=0; i<uindex; i++){
         printf("%s\n", ubuf[i]); 
      }
      printf("\n");
   }
   if (c){
      printf("Commented headers filename(s):\n");
      for (i=0; i<cindex; i++){
         printf("%s\n", cbuf[i]); 
      }
   }
   return 0;
}
