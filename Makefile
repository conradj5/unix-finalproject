SRC = boolexp
CC = gcc -g
LEX = flex
LEX_FLAGS =
YACC = bison
YACC_FLAGS = -d -t

all: interpreter parsetree 
#all: interpreter compiler 

interpreter: lex.yy.o $(SRC).tab.o interpreter.o
	$(CC) -lm lex.yy.o $(SRC).tab.o interpreter.o -o interpreter

compiler: lex.yy.o $(SRC).tab.o compiler.o
	$(CC) lex.yy.o $(SRC).tab.o compiler.o -o compiler

parsetree: lex.yy.o $(SRC).tab.o parsetree.o
	$(CC) lex.yy.o $(SRC).tab.o parsetree.o -o parsetree

lex.yy.o: lex.yy.c $(SRC).tab.h $(SRC).h
	$(CC) -c lex.yy.c

lex.yy.c: $(SRC).l
	$(LEX) $(LEX_FLAGS) $(SRC).l

$(SRC).tab.o: $(SRC).tab.c $(SRC).h
	$(CC) -c $(SRC).tab.c

$(SRC).tab.c: $(SRC).y
	$(YACC) $(YACC_FLAGS) $(SRC).y

$(SRC).tab.h: $(SRC).y
	$(YACC) $(YACC_FLAGS) $(SRC).y

interpreter.o: interpreter.c $(SRC).h $(SRC).tab.h 
	$(CC) -c interpreter.c

compiler.o: compiler.c $(SRC).h $(SRC).tab.h 
	$(CC) -c compiler.c

parsetree.o: parsetree.c $(SRC).h $(SRC).tab.h 
	$(CC) -c parsetree.c

clean:
	-rm *.o $(SRC).tab.h $(SRC).tab.c lex.yy.c interpreter compiler parsetree
