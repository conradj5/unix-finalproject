%{
#include <stdlib.h>
#include <stdio.h>
#include "boolexp.h"
#include <stdarg.h>
#define SIZE 26

PTnode* newOperatorNode(int oper, int nops, ...);
PTnode* newLiteralOrVariableNode(int literalOrVariable, PTnodeFlag flag);
void freePTnode(PTnode* nodePtr);
int dfs(PTnode* nodePtr, char* str);

void yyerror(char* s);

int environment[SIZE];      /* environment */

extern int yylineno;

%}



%union {
   int literal;
   char environI;
   PTnode* nodePtr;
};

%token <literal> LIT
%token <environI> VAR TVAR
%type <nodePtr> declaration varlist exp sentence

%left '|'
%left '&'
%left '~'

%%

sentence: '(' declaration ',' exp ')'    {char* str = (char*)malloc(sizeof(char));
														int result = dfs($4, str);
														printf("%s is %s.\n", str, result ? "true":"false");
														free(str);
														freePTnode($4);}
			|

declaration: '[' varlist ']'             {$$ = $2;}
           | '['']'                      { }

varlist: TVAR                          {PTnode* tnode = newLiteralOrVariableNode($1, variableFlag);
														environment[tnode->literalOrVariable] = 1;
														$$ = tnode;}
			| varlist ',' TVAR            {PTnode* tnode = newLiteralOrVariableNode($3, variableFlag);
														environment[tnode->literalOrVariable] = 1;
														$$ = tnode;}
       
exp: exp '&' exp                         {$$ = newOperatorNode('&', 2, $1, $3);}
      | exp '|' exp                      {$$ = newOperatorNode('|', 2, $1, $3);}
      | '~' exp                          {$$ = newOperatorNode('~', 1, $2);}
      | LIT                              {$$ = newLiteralOrVariableNode($1, literalFlag);}
      | VAR                              {PTnode* tnode = newLiteralOrVariableNode($1, variableFlag);
														if (environment[tnode->literalOrVariable] != 1){
															environment[tnode->literalOrVariable] = 0;
														}
														$$ = tnode;}


%%

void yyerror(char* s){
   fprintf(stderr, "line %d: %s\n", yylineno, s);
}

int main(void){
   yyparse();
   return 0;
}

PTnode* newLiteralOrVariableNode(int lov, PTnodeFlag f){
   PTnode* nodePtr;

   if ((nodePtr = malloc (sizeof (*nodePtr))) == NULL)
      yyerror("out of memory");

   nodePtr->flag = f;
   nodePtr->literalOrVariable = lov;

   return nodePtr;
}

PTnode* newOperatorNode(int ol, int numOfOperands, ...){
   va_list ap;
   PTnode* nodePtr;

   int i;

   if ((nodePtr = malloc (sizeof (*nodePtr))) == NULL)
      yyerror("out of memory");

   nodePtr->flag = operatorFlag;
   nodePtr->operator1.operatorLiteral = ol;
   nodePtr->operator1.numOfOperands = numOfOperands;

   va_start(ap, numOfOperands);

   if (((nodePtr->operator1.operands = malloc (numOfOperands * sizeof (PTnode*)))) == NULL)
      yyerror("out of memory");

   for (i = 0; i < numOfOperands; i++){
      nodePtr->operator1.operands[i] = va_arg (ap, PTnode*);
   }
   va_end (ap);

   return nodePtr;
}

void freePTnode(PTnode* nodePtr) {

   int i;

   if (!nodePtr)
      return;

   if (nodePtr->flag == operatorFlag) {

      for (i = 0; i < nodePtr->operator1.numOfOperands; i++)
         freePTnode(nodePtr->operator1.operands[i]);
      if (nodePtr->operator1.operands)
         free(nodePtr->operator1.operands);
   }

   free(nodePtr); 
}
